FROM node:11.11.0-stretch

RUN mkdir -p /app
WORKDIR /app

RUN apt-get update
RUN apt-get install -y supervisor

RUN npm install -g nodemon

COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf

COPY package*.json ./
RUN npm install

COPY .babelrc ./

EXPOSE 3000

CMD ["/usr/bin/supervisord"]