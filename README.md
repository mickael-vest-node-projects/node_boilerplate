# Node/React boilerplate for development in Docker
A simple personal boilerplate project to develop Node/React (typescript) apps.
This puts the app in a Docker container (linked to actual host files) and hot reloads when anything is changed.

By using several different ones and mapping the ports correctly, it's easy to run several projects, edit them and check the changes in real time.

In some keywords :
- Node + Express
- React
- Babel
- Everything in a Docker container
- Hot reload with Nodemon and Webpack

## Building the image
```bash
# Get to the root directory of the repo
docker build -t node-docker-dev .
```

## Running a container
### Linux :
```bash
docker run -d --rm --name nodedev -p 3000:3000 -e DOCKER_HOST_OS=LINUX -v ~/SomeDir/NodeApp/src:/app/src node-docker-dev
```

### Windows :
```bash
docker run -d --rm --name nodedev -p 3000:3000 -e DOCKER_HOST_OS=WINDOWS -v c:/SomeDir/NodeApp/src:/app/src node-docker-dev
```